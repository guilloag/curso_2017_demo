/*
 Name:		BTMatrixServoSerialDemo.ino
 Created:	3/8/2018 11:05:35 PM
 Author:	Guillermo
*/

#include <DHT_U.h>
#include <DHT.h>
#include <MD_MAX72xx.h>
#include <Servo.h>
#include <CmdParser.hpp>
#include <CmdCallback.hpp>
#include <CmdBuffer.hpp>
#include <SoftwareSerial.h>

#define DHTTYPE DHT11

#define VERSION 2.0

const int servoPin = 2;
const int dataPin = 3;
const int csPin = 4;
const int clkPin = 5;
const int buzzerPin = 6;
const int dht11Pin = 7;
const byte txPin = 8;
const byte rxPin = 9;

// set up a new serial object
SoftwareSerial btSerial(rxPin, txPin);

CmdCallback<7> callback;
char HelloCommand[] = "HOLA";
char QuitCommand[] = "SALIR";
char SetCommand[] = "AJUSTAR";
char ShowCommand[] = "MOSTRAR";
char WaveCommand[] = "SALUDA";
char DarkSideCommand[] = "OSCURO";
char VersionCommand[] = "VERSION";

Servo servo;
MD_MAX72XX matrix = MD_MAX72XX(dataPin, clkPin, csPin, 1);
DHT dht(dht11Pin, DHTTYPE);

void setup() {
	Serial.begin(9600);
	delay(1000);
	btSerial.begin(9600);
	delay(1000);
	matrix.begin();
	dht.begin();
	servo.attach(servoPin);
	pinMode(buzzerPin, OUTPUT);

	callback.addCmd(HelloCommand, &functHallo);
	callback.addCmd(QuitCommand, &functQuit);
	callback.addCmd(SetCommand, &functSet);
	callback.addCmd(ShowCommand, &functShow);
	callback.addCmd(WaveCommand, &functWave);
	callback.addCmd(DarkSideCommand, &darkPlay);
	callback.addCmd(VersionCommand, &functVersion);

	resetMatrix();
	
	setNeutralFace();

	Serial.println("Comandos: ");
	Serial.println("1: HOLA");
	Serial.println("2: AJUSTAR [angulo] (0-180)");
	Serial.println("3: MOSTRAR [temperatura, humedad, caracter]");
	Serial.println("4: SALUDA");
	Serial.println("5: OSCURO");
	Serial.println("6: SALIR");
	Serial.println();
}

CmdBuffer<32> myBuffer;
CmdParser     myParser;
CmdBuffer<32> btBuffer;
CmdParser     btParser;

void loop() {
	if (myBuffer.readFromSerial(&Serial, 100)) {
		if (myParser.parseCmd(&myBuffer) != CMDPARSER_ERROR) {
			// search command in store and call function
			if (callback.processCmd(&myParser)) {
			}
			myBuffer.clear();
		}
	}
	if (btBuffer.readFromSerial(&btSerial, 100)) {
		if (btParser.parseCmd(&btBuffer) != CMDPARSER_ERROR) {
			if (callback.processCmd(&btParser)) {
			}
			btBuffer.clear();
			Serial.println("btBuffer Cleared");
		}
	}
	// Auto Handling
//	callback.loopCmdProcessing(&myParser, &myBuffer, &Serial);
}

void functVersion(CmdParser *myParser) {
	Serial.println(VERSION);
}

void functHallo(CmdParser *myParser) {
	String param = myParser->getCmdParam(1);
	if (param == NULL) {
		Serial.println("Si Ud no me conoce, no lo voy a saludar, estimada persona.");
		blinkNeutralFace();
		return;
	}
	if (param == "ARDUINO") {
		Serial.println("Hola para vos, ameo!");
		clearMouth();
		clearEyes();
		smileMouth();
		normalRight();
		normalLeft();
		delay(100);
		clearEyes();
		blinkRight();
		normalLeft();
		delay(200);
		normalRight();
		return;
	}
	Serial.print("No conozco a ningun ");
	Serial.println(param);
	blinkNeutralFace();
}

void functSet(CmdParser *myParser)
{
	Serial.println("Set en marcha");
	String param = myParser->getCmdParam(1);

	if (param == NULL) {
		Serial.println("Falla! Falto enviar el angulo.");
		return;
	}
	int angle = param.toInt();
	moveServoToAngle(angle);
	Serial.print("Nuevo angulo: ");
	Serial.println(angle);
}

void functQuit(CmdParser *myParser) {
	Serial.println("No te vayas, Chavito.... :'( ");
}

void functShow(CmdParser *myParser) {
	if (myParser->equalCmdParam(1, "temperatura")) {
		Serial.print("La temperatura actual es: ");
		float t = getTemperature();
		Serial.print(t);
		Serial.println(" grados centigrados");
	}
	else if (myParser->equalCmdParam(1, "humedad")) {
		Serial.print("La humedad actual es: ");
		float h = getHumidity();
		Serial.print(h);
		Serial.println(" %");
	}
	else
	{
		Serial.print("Me dijiste que muestre: ");
		Serial.println(myParser->getCmdParam(1));
	}
}

void functWave(CmdParser *myParser) {
	Serial.println(":) Hola!");
	moveServoToAngle(45);
	delay(200);
	moveServoToAngle(135);
	delay(200);
	moveServoToAngle(45);
	delay(200);
	moveServoToAngle(135);
	delay(200);
	moveServoToAngle(45);
	delay(200);
	moveServoToAngle(135);
	delay(200);
	moveServoToAngle(45);
	delay(200);
	moveServoToAngle(135);
}

float getTemperature() {
	return dht.readTemperature();
}

float getHumidity() {
	return dht.readHumidity();
}

void moveServoToAngle(int angle) {
	servo.write(angle);
}

void setNeutralFace() {
	matrix.clear();
	uint8_t face[] = { 0x00, 0x62, 0x62, 0x2, 0x02, 0x62, 0x62, 0x00 };
	for (int i = 1; i < 7; i++) {
		matrix.setRow(0, i, face[i]);
	}
}

void blinkNeutralFace() {
	clearEyes();
	blinkRight();
	blinkLeft();
	delay(100);
	setNeutralFace();
	delay(100);
	clearEyes();
	blinkRight();
	blinkLeft();
	delay(100);
	setNeutralFace();
}

void blinkRight() {
	matrix.setPoint(1, 5, true);
	matrix.setPoint(2, 5, true);
}

void normalRight() {
	matrix.setPoint(1, 5, true);
	matrix.setPoint(2, 5, true);
	matrix.setPoint(1, 6, true);
	matrix.setPoint(2, 6, true);
}

void blinkLeft() {
	matrix.setPoint(5, 5, true);
	matrix.setPoint(6, 5, true);
}

void normalLeft() {
	matrix.setPoint(5, 5, true);
	matrix.setPoint(6, 5, true);
	matrix.setPoint(5, 6, true);
	matrix.setPoint(6, 6, true);
}

void clearEyes() {
	matrix.setPoint(1, 5, false);
	matrix.setPoint(1, 6, false);
	matrix.setPoint(2, 5, false);
	matrix.setPoint(2, 6, false);
	matrix.setPoint(5, 5, false);
	matrix.setPoint(5, 6, false);
	matrix.setPoint(6, 5, false);
	matrix.setPoint(6, 6, false);
}

void clearMouth() {
	matrix.setColumn(0, 0x00);
	matrix.setColumn(1, 0x00);
	matrix.setColumn(2, 0x00);
	matrix.setColumn(3, 0x00);
}

void smileMouth() {
	matrix.setPoint(6, 3, true);
	matrix.setPoint(5, 2, true);
	matrix.setPoint(4, 1, true);
	matrix.setPoint(3, 1, true);
	matrix.setPoint(2, 2, true);
	matrix.setPoint(1, 3, true);
}

void scaredMouth() {
	matrix.setPoint(4, 1, true);
	matrix.setPoint(3, 1, true);
	matrix.setPoint(4, 2, true);
	matrix.setPoint(3, 2, true);
}

void resetMatrix()
{
	matrix.control(MD_MAX72XX::INTENSITY, MAX_INTENSITY / 2);
	matrix.control(MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
	matrix.clear();
}

//Note Definitions
const int NOTE_AS3 = 233;
const int NOTE_C4 = 262;
const int NOTE_D4 = 294;
const int NOTE_E4 = 330;
const int NOTE_F4 = 349;
const int NOTE_G4 = 392;
const int NOTE_GS4 = 415;
const int NOTE_A4 = 440;
const int NOTE_AS4 = 466;
const int NOTE_B4 = 494;
const int NOTE_C5 = 523;
const int NOTE_CS5 = 554;
const int NOTE_D5 = 587;
const int NOTE_DS5 = 622;
const int NOTE_E5 = 659;
const int NOTE_F5 = 699;
const int NOTE_FS5 = 740;
const int NOTE_G5 = 784;
const int NOTE_GS5 = 831;
const int NOTE_A5 = 880;

const int rhythmLength = 115;

int rhythmNotes[] = { NOTE_A4,0,NOTE_A4,NOTE_A4,NOTE_A4,NOTE_A4,0,NOTE_A4,NOTE_A4,NOTE_A4,NOTE_A4,0,NOTE_A4,NOTE_A4,NOTE_A4,NOTE_F4,NOTE_F4,NOTE_F4,NOTE_C5,NOTE_C5,NOTE_C5,  //Intro 1 (21 Notes)
NOTE_A4,0,NOTE_A4,NOTE_A4,NOTE_A4,NOTE_A4,0,NOTE_A4,NOTE_A4,NOTE_A4,NOTE_A4,0,NOTE_A4,NOTE_A4,NOTE_A4,NOTE_F4,NOTE_F4,NOTE_F4,NOTE_C5,NOTE_C5,NOTE_C5,   //Intro 2 (21 Notes)
NOTE_A4,NOTE_A4,NOTE_A4,NOTE_F4,NOTE_C5,NOTE_A4,NOTE_F4,NOTE_C5,NOTE_A4,                                                                                 //Part 1  (9 Notes)
0,NOTE_E5,NOTE_E5,NOTE_E5,NOTE_F5,NOTE_C5,NOTE_GS4,NOTE_F4,NOTE_C5,NOTE_A4,                                                                              //Part 2  (10 Notes)
0,NOTE_A5,NOTE_A4,NOTE_A4,NOTE_A5,NOTE_GS5,NOTE_G5,NOTE_FS5,NOTE_F5,NOTE_FS5,                                                                            //Part 3  (10 Notes)
0,NOTE_AS4,NOTE_DS5,NOTE_D5,NOTE_CS5,NOTE_C5,NOTE_B4,NOTE_C5,                                                                                            //Part 4  (8 Notes)
0,NOTE_F4,NOTE_GS4,NOTE_F4,NOTE_A4,NOTE_C5,NOTE_A4,NOTE_C5,NOTE_E5,                                                                                      //Part 5  (9 Notes)
0,NOTE_A5,NOTE_A4,NOTE_A4,NOTE_A5,NOTE_GS5,NOTE_G5,NOTE_FS5,NOTE_F5,NOTE_FS5,                                                                            //Part 6  (10 Notes)
0,NOTE_AS4,NOTE_DS5,NOTE_D5,NOTE_CS5,NOTE_C5,NOTE_B4,NOTE_C5,                                                                                            //Part 7  (8 Notes)
0,NOTE_F4,NOTE_GS4,NOTE_F4,NOTE_C5,NOTE_A4,NOTE_F4,NOTE_C5,NOTE_A4 };                                                                                     //Part 8  (9 Notes)

int rhythmBeats[] = { 1,2,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,1,1,1,1,   //Intro 1
1,2,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,1,1,1,1,    //Intro 2
4,4,4,3,1,4,3,1,4,                            //Part 1       
4,4,4,4,3,1,4,3,1,4,                          //Part 2
4,4,3,1,4,3,1,1,1,1,                          //Part 3
3,2,4,3,1,1,1,1,                              //Part 4 
3,2,4,3,1,4,3,1,4,                            //Part 5
3,4,3,1,4,3,1,1,1,1,                          //Part 6
3,2,4,3,1,1,1,1,                              //Part 7 
3,2,4,3,1,4,3,1,4 };                           //Part 8

int tempo = 150;

void darkPlay(CmdParser *myParser)
{
	Serial.println("Ven al lado oscuro!!");
	clearMouth();
	scaredMouth();

	int i, rhythmDuration;

	for (i = 0; i < rhythmLength; i++) // step through the song arrays
	{
		rhythmDuration = rhythmBeats[i] * tempo;  // length of note/rest in ms

		if (rhythmNotes[i] == '0')          // is this a rest? 
		{
			delay(rhythmDuration);            // then pause for a moment
		}

		else                          // otherwise, play the note
		{
			tone(buzzerPin, rhythmNotes[i], rhythmDuration);
			delay(rhythmDuration);            // wait for tone to finish
		}
		delay(tempo / 10);              // brief pause between notes
	}
	servo.attach(servoPin);
	setNeutralFace();
}
